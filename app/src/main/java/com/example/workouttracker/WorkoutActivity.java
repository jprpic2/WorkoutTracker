package com.example.workouttracker;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.LiveData;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.example.workouttracker.Room.AppDatabase;
import com.example.workouttracker.Room.Classes.WorkoutDone;
import com.example.workouttracker.WorkoutRecycler.WorkoutAdapter;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;


public class WorkoutActivity extends AppCompatActivity implements WorkoutAdapter.ItemClickListener{

    WorkoutAdapter adapter;
    TextView tvDate;
    Button btnStrength;
    Button btnCardio;
    String strDate;
    AppDatabase mDb;
    LiveData<List<WorkoutDone>> workoutsDone;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.day_workout);
        tvDate=findViewById(R.id.tvDate);
        RecyclerView recyclerView = findViewById(R.id.workoutRecycler);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        adapter = new WorkoutAdapter();
        adapter.setClickListener(this);
        recyclerView.setAdapter(adapter);
        setupButtons();

        Bundle extras = getIntent().getExtras();
        if(extras != null) {
            try {

                // Get date from previous activity, reformat it to dd MMMM yyyy -- ex. 28 August 2021
                DateFormat formatter = new SimpleDateFormat("yyyy/MM/dd", Locale.ENGLISH);
                Date date = formatter.parse(extras.getString("date"));
                formatter = new SimpleDateFormat("dd MMMM yyyy",Locale.ENGLISH);
                strDate = formatter.format(date);
                tvDate.setText(strDate);
                mDb=AppDatabase.getInstance(this);

                // Get LiveData workouts done -- update adapter with each data change
                workoutsDone = mDb.workoutDoneDao().getDoneWorkouts(date);
                workoutsDone.observe(this, workoutDones -> adapter.setWorkoutsDone(workoutDones));

            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onItemClick(View view, int position) {
        Intent intent = new Intent(getApplicationContext(), WorkoutDetailActivity.class);
        intent.putExtra("Name",adapter.getItem(position).getName());
        startActivity(intent);
    }

    @Override
    public void onButtonClick(View view, int position) {
        adapter.removeItem(position,WorkoutActivity.this);
    }


    private void setupButtons(){
        btnStrength = findViewById(R.id.btnStrength);
        btnCardio=findViewById(R.id.btnCardio);

        btnStrength.setOnClickListener(view -> {
            Intent intent = new Intent(getApplicationContext(), AddStrengthWorkout.class);
            intent.putExtra("date", strDate);
            startActivity(intent);
        });

        btnCardio.setOnClickListener(view -> {
            Intent intent = new Intent(getApplicationContext(), AddCardioWorkout.class);
            intent.putExtra("date", strDate);
            startActivity(intent);
        });
    }

}