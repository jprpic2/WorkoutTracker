package com.example.workouttracker.WorkoutDetailRecycler;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.workouttracker.R;
import com.example.workouttracker.Room.Classes.WorkoutDetail;

import java.util.ArrayList;
import java.util.List;

public class WorkoutDetailAdapter extends RecyclerView.Adapter<WorkoutDetailAdapter.WorkoutDetailViewHolder>{

    private List<WorkoutDetail> workouts;

    public WorkoutDetailAdapter() {
        this.workouts = new ArrayList<>();
    }

    public void setWorkouts(List<WorkoutDetail> workouts) {
        this.workouts = workouts;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public WorkoutDetailViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.workout_step, parent, false);

        return new WorkoutDetailAdapter.WorkoutDetailViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull WorkoutDetailViewHolder holder, int position) {
        holder.setWorkoutStep(workouts.get(position).getStepNumber());
        holder.setWorkoutDescription(workouts.get(position).getStepDescription());
    }

    @Override
    public int getItemCount() {
        return this.workouts.size();
    }

    public class WorkoutDetailViewHolder extends RecyclerView.ViewHolder {
        private final TextView tvStep;
        private final TextView tvDescription;

        public WorkoutDetailViewHolder(@NonNull View itemView) {
            super(itemView);
            tvStep = itemView.findViewById(R.id.tvStepNumber);
            tvDescription = itemView.findViewById(R.id.tvStepDescription);
        }
        public void setWorkoutStep(String StepNumber) {
            this.tvStep.setText(StepNumber);
        }
        public void setWorkoutDescription(String description){
            this.tvDescription.setText(description);
        }

    }
}
