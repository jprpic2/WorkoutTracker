package com.example.workouttracker.Room.Data;

import com.example.workouttracker.Room.Classes.WorkoutDetail;

public class WorkoutsHowToData {
    public static WorkoutDetail[] populateHowTos() {
        return new WorkoutDetail[]{
                new WorkoutDetail("Bicep Curl","1","Start standing with a dumbbell in each hand. Your elbows should rest at your sides and your forearms should extend out in front of your body. Your knees should stay slightly bent and your belly button should draw into the spine."),
                new WorkoutDetail("Bicep Curl","2","Bring the dumbbells all the way up to your shoulders by bending your elbows. Once at the top, hold for a second by squeezing the muscle."),
                new WorkoutDetail("Bicep Curl","3","Reverse the curl slowly and repeat."),


                new WorkoutDetail("Hammer Curl","1","Stand up straight with a dumbbell in each hand, holding them alongside you. Your palms should face your body. Keep your feet hip-width apart and engage your core to stabilize the body."),
                new WorkoutDetail("Hammer Curl","2","Keep your biceps stationary and start bending at your elbows, lifting both dumbbells."),
                new WorkoutDetail("Hammer Curl","3","Lift until the dumbbells reach shoulder-level, but don’t actually touch your shoulders. Hold this contraction briefly, then lower back to the starting position and repeat."),

                new WorkoutDetail("Plank","1","Begin on your hands and knees, with hands shoulder-width apart and knees hip-width apart."),
                new WorkoutDetail("Plank","2","Straighten your legs out behind you and rise onto the balls of your feet. Keep your arms straight, but not locked, with your shoulders aligned over your hands. Your body should be in a straight diagonal line from your shoulders to your toes."),

                new WorkoutDetail("Push-up","1","Begin in a plank position with your hands stacked under your shoulders and your body in a straight line."),
                new WorkoutDetail("Push-up","2","Slowly lower your body to the ground, keeping your neck, back and legs aligned. Keep your hips square to the floor as you lower yourself to the ground. Press back up to plank and repeat."),

                new WorkoutDetail("Pull-up","1","Start with your hands on the bar approximately shoulder-width apart with your palms facing forward. "),
                new WorkoutDetail("Pull-up","2","With arms extended above you, stick your chest out and curve your back slightly. That is your starting position. "),
                new WorkoutDetail("Pull-up","3","Pull yourself up towards the bar using your back until the bar is at chest level while breathing out. "),
                new WorkoutDetail("Pull-up","4","Slowly lower yourself to the starting position while breathing in. That is one rep."),

                new WorkoutDetail("Shoulder Press","1","Holding a dumbbell in each hand, stand with your feet hip-width apart. Bend your elbows out to the side and raise your arms to shoulder height, creating a cactus shape with your limbs. The dumbbells should be at ear-level."),
                new WorkoutDetail("Shoulder Press","2","Push the dumbbells up until your arms are almost fully vertically extended, but make sure not to lock your elbows. Slowly lower the dumbbells back down to the starting position, then repeat."),

                new WorkoutDetail("Jumping-Jacks","1","Start standing with your feet together and arms at your sides."),
                new WorkoutDetail("Jumping-Jacks","2","Jump so your feet are approximately three feet apart while raising your arms to a relaxed “V” shape. Jump back to the starting position and repeat."),

                new WorkoutDetail("Squats","1","Stand with your feet shoulder-width apart and your arms straight overhead. Push your hips backward and sit toward your heels, keeping your core tight, back straight and upper body upright. Once your hips are parallel with your knees, drive your hips forward and rise back to a standing position."),

                new WorkoutDetail("Lunges","1","Stand straight with feet shoulder-width apart and parallel."),
                new WorkoutDetail("Lunges","2","Step one leg forward and lower your hips until both knees reach a 90-degree angle. Your back heel should come off the floor. Clasp your hands together at your chest. Rise back to your starting position and repeat."),

                new WorkoutDetail("Crunches","1","Lie on your back with your knees bent and feet flat on the floor, hip-width apart. Make sure your lower back is pressed to the mat and there is no space between your back and the mat. Place your hands behind your head so your thumbs are behind your ears."),
                new WorkoutDetail("Crunches","2","Pulling your abs inward, curl up and forward so your head, neck and shoulder blades are lifted off of the floor. Always lead with your chest as if there is a band on it, pulling you upwards. Do not lead with elbows or chin. Repeat the exercise."),

                new WorkoutDetail("Diamond Push-up","1","Start in a plank position on all fours with your legs extended behind you and your hands stacked directly under your shoulders."),
                new WorkoutDetail("Diamond Push-up","2","Position your hands close together, spreading your fingers to create a diamond shape with your index fingers."),
                new WorkoutDetail("Diamond Push-up","3","Begin to lower your chest to the floor while keeping your back flat."),
                new WorkoutDetail("Diamond Push-up","4","Press back up into a full arm extension and repeat.")
        };
    }
}
