package com.example.workouttracker.Room.Classes;

import androidx.annotation.NonNull;
import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;
import androidx.room.TypeConverters;

import com.example.workouttracker.Room.DateConverter;

import java.util.Date;

@Entity(tableName = "workout_done",foreignKeys = {@ForeignKey(entity = Workout.class,
        parentColumns = "name",
        childColumns = "name")
})
@TypeConverters(DateConverter.class)
public class WorkoutDone {
    @PrimaryKey(autoGenerate = true)
    private int id;
    @NonNull
    private String name;
    private Date date;
    private Integer duration;
    private Integer sets;
    private Integer reps;

    public WorkoutDone(@NonNull String name,Date date, Integer duration, Integer sets, Integer reps) {
        this.name = name;
        this.date = date;
        this.duration = duration;
        this.sets = sets;
        this.reps = reps;
    }

    @Ignore
    public WorkoutDone(@NonNull String name, Date date, Integer duration) {
        this.name = name;
        this.date = date;
        this.duration = duration;
        this.sets = null;
        this.reps = null;
    }

    @Ignore
    public WorkoutDone(@NonNull String name, Date date, Integer sets, Integer reps) {
        this.name = name;
        this.date = date;
        this.sets = sets;
        this.reps = reps;
        this.duration=null;
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    @NonNull
    public String getName() {
        return name;
    }

    public void setName(@NonNull String name) {
        this.name = name;
    }

    public Integer getDuration() {
        return duration;
    }

    public void setDuration(Integer duration) {
        this.duration = duration;
    }

    public Integer getSets() {
        return sets;
    }

    public void setSets(Integer sets) {
        this.sets = sets;
    }

    public Integer getReps() {
        return reps;
    }

    public void setReps(Integer reps) {
        this.reps = reps;
    }
}
