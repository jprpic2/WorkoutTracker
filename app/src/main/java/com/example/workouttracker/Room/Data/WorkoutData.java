package com.example.workouttracker.Room.Data;

import com.example.workouttracker.Room.Classes.Workout;

public class WorkoutData {
    public static Workout[] populateWorkouts() {
        return new Workout[]{
            new Workout("Bicep Curl","Strength","Beginner"),
                new Workout("Hammer Curl","Strength","Beginner"),
                new Workout("Plank","Cardio","Beginner"),
                new Workout("Push-up","Strength","Intermediate"),
                new Workout("Pull-up","Strength","Intermediate"),
                new Workout("Shoulder Press","Strength","Beginner"),
                new Workout("Jumping-Jacks","Cardio","Beginner"),
                new Workout("Squats","Strength","Beginner"),
                new Workout("Lunges","Cardio","Beginner"),
                new Workout("Crunches","Strength","Beginner"),
                new Workout("Diamond Push-up","Strength","Hard")
        };
    }
}
