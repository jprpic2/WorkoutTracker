package com.example.workouttracker.Room.Classes;

import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.PrimaryKey;

@Entity(tableName = "workout_detail",foreignKeys = {@ForeignKey(entity = Workout.class,
        parentColumns = "name",
        childColumns = "workoutName",
        onDelete = ForeignKey.CASCADE)})
public class WorkoutDetail {
    @PrimaryKey(autoGenerate = true)
    private int id;
    private String workoutName;
    private String stepNumber;
    private String stepDescription;

    public WorkoutDetail(String workoutName, String stepNumber, String stepDescription) {
        this.workoutName = workoutName;
        this.stepNumber = stepNumber;
        this.stepDescription = stepDescription;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getWorkoutName() {
        return workoutName;
    }

    public void setWorkoutName(String workoutName) {
        this.workoutName = workoutName;
    }

    public String getStepNumber() {
        return stepNumber;
    }

    public void setStepNumber(String stepNumber) {
        this.stepNumber = stepNumber;
    }

    public String getStepDescription() {
        return stepDescription;
    }

    public void setStepDescription(String stepDescription) {
        this.stepDescription = stepDescription;
    }
}
