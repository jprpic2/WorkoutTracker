package com.example.workouttracker;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.widget.TextView;

import com.example.workouttracker.Room.AppDatabase;
import com.example.workouttracker.Room.AppExecutors;
import com.example.workouttracker.Room.Classes.Workout;
import com.example.workouttracker.Room.Classes.WorkoutDetail;
import com.example.workouttracker.WorkoutDetailRecycler.WorkoutDetailAdapter;

import java.util.List;

public class WorkoutDetailActivity extends AppCompatActivity {

    WorkoutDetailAdapter adapter;
    public TextView workoutName;
    public TextView workoutDifficulty;
    AppDatabase mDb;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.workout_detail);
        workoutName = findViewById(R.id.tvDetailworkoutName);
        workoutDifficulty = findViewById(R.id.tvDifficulty);
        mDb= AppDatabase.getInstance(WorkoutDetailActivity.this);

        // Set Up RecyclerView
        RecyclerView recyclerView = findViewById(R.id.workoutStepsRecycler);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        adapter = new WorkoutDetailAdapter();
        recyclerView.setAdapter(adapter);

        Bundle extras = getIntent().getExtras();
        if(extras != null) {

            // Get chosen workout name
            String name = extras.getString("Name");
            AppExecutors.getInstance().diskIO().execute(() -> {
                Workout workout = mDb.workoutDao().getWorkout(name);

                // Update UI
                workoutName.setText(workout.getName());
                workoutDifficulty.setText(workout.getDifficulty());
            });
            AppExecutors.getInstance().diskIO().execute(() -> {

                // Get and set workout how to steps
                List<WorkoutDetail> workoutDetails = mDb.workoutDetail().getWorkoutDetails(name);
                adapter.setWorkouts(workoutDetails);
            });
        }

    }
}