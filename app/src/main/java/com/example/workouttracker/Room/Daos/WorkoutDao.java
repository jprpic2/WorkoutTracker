package com.example.workouttracker.Room.Daos;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;
import com.example.workouttracker.Room.Classes.Workout;
import java.util.List;

@Dao
public interface WorkoutDao {
    @Insert
    void insertAll(Workout... obj);

    @Query("SELECT name from workout where type = 'Cardio'")
    List<String> getCardioWorkouts();

    @Query("SELECT * from workout where name = :name")
    Workout getWorkout(String name);

    @Query("SELECT name from workout where type = 'Strength'")
    List<String> getStrengthWorkouts();

}
