package com.example.workouttracker;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import com.example.workouttracker.Room.AppDatabase;
import com.example.workouttracker.Room.AppExecutors;
import com.example.workouttracker.Room.Classes.WorkoutDone;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class AddStrengthWorkout extends AppCompatActivity {
    Date date;
    Button btnChooseWorkout;
    EditText etSets;
    EditText etReps;
    TextView tvChosenWorkoutName;
    Button btnSubmit;
    String chosenWorkout;
    AppDatabase mDb;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_strength_workout);
        setUpViews();
        setUpData();
        setUpInsertButton();

        Bundle extras = getIntent().getExtras();
        String strDate;
        if(extras != null) {
            // Get date
            strDate = extras.getString("date");
            DateFormat format = new SimpleDateFormat("d MMMM yyyy", Locale.ENGLISH);
            try {
                date = format.parse(strDate);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
    }

    private void setUpViews() {
        btnChooseWorkout=findViewById(R.id.btnChoose);
        etSets=findViewById(R.id.etSets);
        etReps=findViewById(R.id.etReps);
        tvChosenWorkoutName=findViewById(R.id.tvChosenWorkoutName);
        btnSubmit=findViewById(R.id.btnAddStrength);
    }

    private void setUpInsertButton() {
        btnSubmit.setOnClickListener(view -> {
            if(!TextUtils.isEmpty(etReps.getText()) && !TextUtils.isEmpty(etSets.getText()) && !TextUtils.isEmpty(chosenWorkout)){
                AppExecutors.getInstance().diskIO().execute(() -> {
                    // Insert new Workout Done and close current activity
                    WorkoutDone workoutDone = new WorkoutDone(chosenWorkout,date,Integer.parseInt(etSets.getText().toString()),Integer.parseInt(etReps.getText().toString()));
                    mDb.workoutDoneDao().insert(workoutDone);
                    finish();
                });
            }
        });

    }

    private void setUpData() {
        mDb = AppDatabase.getInstance(this);
        AppExecutors.getInstance().diskIO().execute(() -> {
            List<String> availableWorkouts = mDb.workoutDao().getStrengthWorkouts();
            setUpAlertDialog(availableWorkouts);
        });
    }

    private void setUpAlertDialog(List<String> workouts){
        // Nothing is selected when AlertDialog is created
        final int[] checkedItem = {-1};


        btnChooseWorkout.setOnClickListener(v -> {

            AlertDialog.Builder alertDialog = new AlertDialog.Builder(AddStrengthWorkout.this);
            alertDialog.setTitle("Choose a Workout");
            String[] listItems = workouts.toArray(new String[0]);
            alertDialog.setSingleChoiceItems(listItems, checkedItem[0], (dialog, which) -> {

                // Remember selected item
                checkedItem[0] = which;

                // Update chosen workout TextView
                chosenWorkout = listItems[which];
                tvChosenWorkoutName.setText("Selected Workout is : " + chosenWorkout);
                dialog.dismiss();
            });

            alertDialog.setNegativeButton("Cancel", (dialog, which) -> {
                // Close AlertDialog
            });

            AlertDialog customAlertDialog = alertDialog.create();
            customAlertDialog.show();
        });
    }
}