package com.example.workouttracker;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.text.TextUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.example.workouttracker.Room.AppDatabase;
import com.example.workouttracker.Room.AppExecutors;
import com.example.workouttracker.Room.Classes.WorkoutDone;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class AddCardioWorkout extends AppCompatActivity {
    Date date;
    Button btnChooseWorkout;
    EditText etHours;
    EditText etMins;
    EditText etSecs;
    TextView tvChosenWorkoutName;
    Button btnSubmit;
    String chosenWorkout;
    AppDatabase mDb;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_cardio_workout);
        setUpViews();
        setUpData();


        Bundle extras = getIntent().getExtras();
        String strDate;
        if(extras != null) {
            // Get date
            strDate = extras.getString("date");
            DateFormat format = new SimpleDateFormat("d MMMM yyyy", Locale.ENGLISH);
            try {
                date = format.parse(strDate);
                setUpInsertButton();
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }

    }

    private void setUpInsertButton() {
        btnSubmit.setOnClickListener(view -> {
            if(!TextUtils.isEmpty(chosenWorkout)){
                AppExecutors.getInstance().diskIO().execute(() -> {
                    // Insert new Workout Done and close current activity
                    int duration=0;
                    if(!TextUtils.isEmpty(etHours.getText())){
                        duration += Integer.parseInt(etHours.getText().toString()) * 3600;
                    }
                    if(!TextUtils.isEmpty(etMins.getText())){
                        duration += Integer.parseInt(etMins.getText().toString()) * 60;
                    }
                    if(!TextUtils.isEmpty(etSecs.getText())){
                        duration += Integer.parseInt(etSecs.getText().toString());
                    }
                    WorkoutDone workoutDone = new WorkoutDone(chosenWorkout,date,duration);
                    mDb.workoutDoneDao().insert(workoutDone);
                    finish();
                });
            }
        });

    }

    private void setUpData() {
        mDb=AppDatabase.getInstance(this);
        AppExecutors.getInstance().diskIO().execute(() -> {
            List<String> availableWorkouts = mDb.workoutDao().getCardioWorkouts();
            setUpAlertDialog(availableWorkouts);
        });
    }

    private void setUpViews() {
        btnChooseWorkout=findViewById(R.id.btnChooseCardio);
        etHours=findViewById(R.id.etHours);
        etMins=findViewById(R.id.etMins);
        tvChosenWorkoutName=findViewById(R.id.tvChosenCardioWorkoutName);
        btnSubmit=findViewById(R.id.btnAddCardio);
        etSecs = findViewById(R.id.etSec);
    }

    private void setUpAlertDialog(List<String> availableWorkouts){
        // Nothing is selected when AlertDialog is created
        final int[] checkedItem = {-1};


        btnChooseWorkout.setOnClickListener(v -> {

            AlertDialog.Builder alertDialog = new AlertDialog.Builder(AddCardioWorkout.this);
            alertDialog.setTitle("Choose a Workout");
            String[] listItems = availableWorkouts.toArray(new String[0]);
            alertDialog.setSingleChoiceItems(listItems, checkedItem[0], (dialog, which) -> {

                // Remember selected item
                checkedItem[0] = which;

                // Update chosen workout TextView
                chosenWorkout = listItems[which];
                tvChosenWorkoutName.setText("Selected Workout is : " + chosenWorkout);
                dialog.dismiss();
            });

            alertDialog.setNegativeButton("Cancel", (dialog, which) -> {
                // Close AlertDialog
            });

            AlertDialog customAlertDialog = alertDialog.create();
            customAlertDialog.show();
        });
    }
}