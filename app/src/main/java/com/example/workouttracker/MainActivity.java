package com.example.workouttracker;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.CalendarView;
import android.widget.TextView;

import java.text.DateFormat;

public class MainActivity extends AppCompatActivity {

    public CalendarView calendar;
    public TextView textView;
    public DateFormat dateFormat;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        this.calendar = findViewById(R.id.calendarView);
        this.textView = findViewById(R.id.tvName);
        dateFormat = DateFormat.getDateInstance(DateFormat.MEDIUM);

        calendar.setOnDateChangeListener((calendarView, year, month, dayOfMonth) -> {
            Intent intent = new Intent(getApplicationContext(), WorkoutActivity.class);
            intent.putExtra("date", year + "/" + month + "/" + dayOfMonth);
            startActivity(intent);
        });
    }


}