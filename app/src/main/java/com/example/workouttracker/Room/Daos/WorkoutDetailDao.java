package com.example.workouttracker.Room.Daos;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import com.example.workouttracker.Room.Classes.WorkoutDetail;

import java.util.List;

@Dao
public interface WorkoutDetailDao {
    @Insert
    void insertAll(WorkoutDetail... obj);

    @Query("SELECT * from workout_detail where workoutName = :name")
    List<WorkoutDetail> getWorkoutDetails(String name);

}
