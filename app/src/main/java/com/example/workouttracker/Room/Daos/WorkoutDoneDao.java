package com.example.workouttracker.Room.Daos;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.TypeConverters;

import com.example.workouttracker.Room.Classes.WorkoutDone;
import com.example.workouttracker.Room.DateConverter;

import java.util.Date;
import java.util.List;

@Dao
@TypeConverters(DateConverter.class)
public interface WorkoutDoneDao {
    @Insert
    void insert(WorkoutDone obj);

    @Query("SELECT * from workout_done WHERE date = :date")
    LiveData<List<WorkoutDone>> getDoneWorkouts(Date date);

    @Delete
    void removeItem(WorkoutDone workoutDone);
}
