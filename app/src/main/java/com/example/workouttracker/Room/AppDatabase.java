package com.example.workouttracker.Room;

import android.content.Context;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.sqlite.db.SupportSQLiteDatabase;

import com.example.workouttracker.Room.Classes.Workout;
import com.example.workouttracker.Room.Classes.WorkoutDetail;
import com.example.workouttracker.Room.Classes.WorkoutDone;
import com.example.workouttracker.Room.Daos.WorkoutDao;
import com.example.workouttracker.Room.Daos.WorkoutDetailDao;
import com.example.workouttracker.Room.Daos.WorkoutDoneDao;
import com.example.workouttracker.Room.Data.WorkoutData;
import com.example.workouttracker.Room.Data.WorkoutsHowToData;

@Database(entities = {WorkoutDone.class, WorkoutDetail.class, Workout.class}, version = 1, exportSchema = false)
public abstract class AppDatabase extends RoomDatabase {

    private static final String LOG_TAG = AppDatabase.class.getSimpleName();
    private static final Object LOCK = new Object();
    private static final String DATABASE_NAME = "workouts.db";
    private static AppDatabase mDb;

    public static AppDatabase getInstance(Context context) {
        if (mDb == null) {
            synchronized (LOCK) {
                Log.d(LOG_TAG, "Creating new database instance");
                mDb = Room.databaseBuilder(context.getApplicationContext(),
                        AppDatabase.class, AppDatabase.DATABASE_NAME)
                        .addCallback(new Callback() {
                            // Pre-populate database with data from WorkoutData and WorkoutsHowToData
                            @Override
                            public void onCreate(@NonNull SupportSQLiteDatabase db) {
                                super.onCreate(db);
                                AppExecutors.getInstance().diskIO().execute(() -> populateDatabase(getInstance(context)));
                            }
                        })
                        .build();
            }
        }
        return mDb;
    }

    private static void populateDatabase(AppDatabase instance) {
        instance.workoutDao().insertAll(WorkoutData.populateWorkouts());
        instance.workoutDetail().insertAll(WorkoutsHowToData.populateHowTos());
    }

    public abstract WorkoutDao workoutDao();
    public abstract WorkoutDoneDao workoutDoneDao();
    public abstract WorkoutDetailDao workoutDetail();
}
