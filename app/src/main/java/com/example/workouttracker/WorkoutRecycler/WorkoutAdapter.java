package com.example.workouttracker.WorkoutRecycler;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.workouttracker.R;
import com.example.workouttracker.Room.AppDatabase;
import com.example.workouttracker.Room.AppExecutors;
import com.example.workouttracker.Room.Classes.WorkoutDone;

import java.util.ArrayList;
import java.util.List;

public class WorkoutAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private List<WorkoutDone> workoutDones;
    private ItemClickListener mClickListener;

    public WorkoutAdapter() {
        this.workoutDones = new ArrayList<>();
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view;
        if (viewType == 0) {
            view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.workout_strength_viewholder, parent, false);
            return new WorkoutStrengthViewHolder(view);
        }
        view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.workout_cardio_viewholder, parent, false);
        return new WorkoutCardioViewHolder(view);
    }

    @Override
    public int getItemViewType(int position) {
        if(workoutDones.get(position).getDuration() == null){
            return 0;
        }
        else{
            return 1;
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        switch (holder.getItemViewType()) {
            case 0:
                // If Strength workout -> fill Strength Workout Viewholder
                WorkoutStrengthViewHolder viewHolder0 = (WorkoutStrengthViewHolder)holder;
                WorkoutDone workoutDoneStrength = workoutDones.get(position);

                viewHolder0.setWorkoutName(workoutDoneStrength.getName());
                viewHolder0.setSets(workoutDoneStrength.getSets());
                viewHolder0.setReps(workoutDoneStrength.getReps());
                break;
            case 1:
                // If Cardio workout -> fill Cardio Workout Viewholder
                WorkoutCardioViewHolder viewHolder2 = (WorkoutCardioViewHolder)holder;
                WorkoutDone workoutDoneCardio = workoutDones.get(position);

                viewHolder2.setWorkoutName(workoutDoneCardio.getName());
                viewHolder2.setDuration(workoutDoneCardio.getDuration());
                break;
        }
    }

    public void setWorkoutsDone(List<WorkoutDone> workoutDones){
        this.workoutDones = workoutDones;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return this.workoutDones.size();
    }

    public void removeItem(int position,Context context) {
        AppExecutors.getInstance().diskIO().execute(() -> {
            AppDatabase mDb = AppDatabase.getInstance(context);
            // Remove item from database, adapter updates because of LiveData onChange
            WorkoutDone workout = workoutDones.get(position);
            mDb.workoutDoneDao().removeItem(workout);
        });
    }

    public class WorkoutCardioViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private final TextView tvWorkoutName;
        private final TextView tvDuration;
        private final ImageButton btnDelete;

        public WorkoutCardioViewHolder(@NonNull View itemView) {
            super(itemView);
            tvWorkoutName = itemView.findViewById(R.id.tvCardioName);
            tvDuration = itemView.findViewById(R.id.tvDuration);
            btnDelete = itemView.findViewById(R.id.btn_cardio_delete);
            btnDelete.setOnClickListener(this);
            itemView.setOnClickListener(this);
        }
        public void setWorkoutName(String workoutName) {
            this.tvWorkoutName.setText(workoutName);
        }

        public void setDuration(int duration) {
            StringBuilder stringBuilder = new StringBuilder();
            if(duration>=3600){
                stringBuilder.append(duration / 3600);
                stringBuilder.append("h ");
                duration = duration % 3600;
            }
            if(duration>=60){
                stringBuilder.append(duration / 60);
                stringBuilder.append("min ");
                duration = duration % 60;
            }
            if(duration>0){
                stringBuilder.append(duration);
                stringBuilder.append("sec");
            }
            this.tvDuration.setText(stringBuilder.toString());
        }

        @Override
        public void onClick(View view) {
            if (mClickListener != null){
                if(view == btnDelete){
                    mClickListener.onButtonClick(view,getAdapterPosition()); }
                else{
                    mClickListener.onItemClick(view, getAdapterPosition()); }
            }
        }
    }

    public class WorkoutStrengthViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private final TextView tvWorkoutName;
        private final TextView tvSets;
        private final TextView tvReps;
        private final ImageButton btnDelete;

        public WorkoutStrengthViewHolder(@NonNull View itemView) {
            super(itemView);
            tvWorkoutName = itemView.findViewById(R.id.tvStrengthName);
            tvSets=itemView.findViewById(R.id.tvSets);
            tvReps = itemView.findViewById(R.id.tvReps);
            btnDelete=itemView.findViewById(R.id.btn_strength_delete);
            btnDelete.setOnClickListener(this);
            itemView.setOnClickListener(this);
        }
        public void setWorkoutName(String workoutName) {
            this.tvWorkoutName.setText(workoutName);
        }

        public void setSets(int sets) {
            this.tvSets.setText(String.valueOf(sets));
        }

        public void setReps(int reps) {
            this.tvReps.setText(String.valueOf(reps));
        }

        @Override
        public void onClick(View view) {
            if (mClickListener != null){
                if(view == btnDelete){
                    mClickListener.onButtonClick(view,getAdapterPosition()); }
            else{
                mClickListener.onItemClick(view, getAdapterPosition()); }
            }
        }
    }

    // Convenience method for getting data at click position
    public WorkoutDone getItem(int id) {
        return workoutDones.get(id);
    }

    // Allows clicks events to be caught
    public void setClickListener(ItemClickListener itemClickListener) {
        this.mClickListener = itemClickListener;
    }

    // Parent activity will implement this method to respond to click events
    public interface ItemClickListener {
        // Go to Workout Details
        void onItemClick(View view, int position);
        // Remove Workout Done
        void onButtonClick(View view,int position);
    }

}
